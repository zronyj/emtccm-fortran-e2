module energy

implicit none

! Establishing the subroutines which will be provided to the external program
public :: short_range, long_range, read_params

contains

	subroutine short_range(r, De, B, r0, Vsr)
	! Special subroutine to calculate the short-range interaction of a distance vector ----
	! Inputs
	! - r: array with the distances
	! - De: constant
	! - B: constant
	! - r0: constant (average distance between two atoms)
	! Outputs
	! - Vsr: array with all short-range interactions at the distances provided by r
	! Description
	! This subroutine computes the short-range potential for an array of distances.
	! -------------------------------------------------------------------------------------

		implicit none

		! Internal variables
		integer :: i

		! In and out variables
		real, intent(in) :: r(:), De, B, r0
		real, allocatable, intent(out) :: Vsr(:)

		! Memory allocation and calculation of the interaction
		allocate(Vsr(size(r)))
		do i = 1, size(r)
			Vsr(i) = De * ( 1 - EXP(B*(r0 - r(i))))**2 - De
		end do

	end subroutine short_range

	subroutine long_range(r, A, C, D, r0, Vlr)
	! Special subroutine to calculate the long-range interaction of a distance vector -----
	! Inputs
	! - r: array with the distances
	! - A: constant
	! - C: constant
	! - D: constant
	! - r0: constant (average distance between two atoms)
	! Outputs
	! - Vlr: array with all long-range interactions at the distances provided by r
	! Description
	! This subroutine computes the long-range potential for an array of distances.
	! -------------------------------------------------------------------------------------

	implicit none

		! Internal variables
		integer :: i
		real :: rP

		! In and out variables
		real, intent(in) :: r(:), A, C, D, r0
		real, allocatable, intent(out) :: Vlr(:)

		! Memory allocation and calculation of the interaction
		allocate(Vlr(size(r)))
		do i = 1, size(r)
			rP = r(i) + C * EXP(r0 - r(i)) ! r prime vector computed as required
			Vlr(i) = (-1)*(A/rP**6 + D/rP**8)
		end do

	end subroutine long_range

	subroutine read_params(filnam, De, A, B, C, D, r0)
	! Special subroutine to get the parameters from a provided file -----------------------
	! Inputs
	! - fil_nam: name of the file containing the parameters to be read
	! Outputs
	! - De: constant
	! - A: constant
	! - B: constant
	! - C: constant
	! - D: constant
	! - r0: constant (average distance between two atoms)
	! Description
	! The subroutine opens a file, checks that the number of rows is equal 6 (the number of
	! parameters) and, if so, it proceeds to read all them in the order described in the
	! README.
	! -------------------------------------------------------------------------------------

	implicit none

	! Internal variables
	character(len=100) :: buf
	integer :: filas, stat, i
	real, dimension(6) :: param_vec

	! In and Out variables
	character(len=255), intent(in) :: filnam
	real, intent(out) :: De, A, B, C, D, r0

	! Open the file
	open(40, file=filnam, status='old')
	
	! Check whether the number of parameters is 6
	filas = 0
	do
		read(40, *, iostat=stat) buf
		if (stat < 0) exit
		filas = filas + 1
	end do
	rewind(40)

	if (filas .ne. 6) then
		! If the user didn't include 6 parameters in the file, use the defaults
		write(*, *) 'The number of parameters in the file is not correct: ', filnam
		write(*, *) 'Expected 6, got ', filas
		write(*, *) 'Please check your file, and make sure it follows the description in the README.'
		write(*, *) "The program will work with the default set of parameters."
		De = 4.618
		A = 7.500
		B = 1.869
		C = 5.000
		D = 125.0
		r0 = 1.275
	else
		! If he did include 6 parameters, read the file
		read(40, *) (param_vec(i), i=1, filas)
		
		! Assign all parameters to the corresponding variables
		De = param_vec(1)
		A = param_vec(2)
		B = param_vec(3)
		C = param_vec(4)
		D = param_vec(5)
		r0 = param_vec(6)
	end if

	! Close the file
	close(40)

	end subroutine read_params

end module energy