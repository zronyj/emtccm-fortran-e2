# Makefile for Exercise 2

exercise2: energy.o exercise2.o
	gfortran exercise2.o energy.o -o exercise2.exe
	chmod +x exercise2.exe
	chmod +x tests.sh
	mkdir source && mv *.f90 ./source/
	rm -f *.o
	rm -f *.mod

exercise2.o: exercise2.f90
	gfortran -c exercise2.f90 -o exercise2.o

energy.o: energy.f90
	gfortran -c energy.f90 -o energy.o

.PHONY: clean

clean:
	mv source/*.f90 ./
	rmdir source
	rm *.exe & rm *.csv