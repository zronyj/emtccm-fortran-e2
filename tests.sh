#!/bin/bash

mkdir Tests
for p in `seq -f "%02g" 0 99`
do
	dname="param0$p"
	mkdir $dname
	cp ./exercise2.exe ./$dname/exercise2.exe
	cp ./Parameters/params0$p.dat ./$dname/params.dat
	cd $dname
	if ./exercise2.exe params.dat > test-param.log
	then
		echo -e "\n----------------------------------------------------------"
		echo "* Test with parameter file params0$p.dat was successful!"
		echo "----------------------------------------------------------"
	else
		echo -e "\n----------------------------------------------------------"
		echo "X Test with parameter file params0$p.dat FAILED!"
		echo "X Please check the log file in $dname"
		echo "----------------------------------------------------------"
	fi
	rm exercise2.exe
	cd ../
	mv $dname ./Tests/
done