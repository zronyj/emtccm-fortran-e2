program exercise2

! -----------------------------------------------------------------------------------------
! This program computes the potential energy of an atom and a charge/molecule at different
! distances using a Morse and a special 6-8 Lennard-Jones potentials ----------------------
! -----------------------------------------------------------------------------------------
!
! Inputs:
! - (optional) A file containing the parameters to be changed for the potentials
!
! Outputs:
! - A CSV file with the distances and the total potential energy at those distances
!
! Special considerations:
! - It wasn't really necessary to store the values for the energies in a dedicated array or
!   vector, but I did it since it was required for the exercise.
! - I chose to read the parameters form a file, since this allows the program to be used
!   by other programs. Especially through the command line.
! -----------------------------------------------------------------------------------------

! Import the modules to calculate the short- and long-range interactions ------------------
use energy, only : short_range, long_range, read_params

implicit none

! Constants -------------------------------------------------------------------------------
real, parameter :: init_val = 0.8
real, parameter :: max_val = 20.0
integer, parameter :: steps = 200

! Variables -------------------------------------------------------------------------------
integer :: i
real :: step, De, A, B, C, D, r0
character(len=255) :: file_name
real, dimension(steps) :: distance_vector, energy_vector
real, allocatable :: Vsr(:), Vlr(:)

! Explaining the program to the user ------------------------------------------------------
write(*, *) "************ Welcome to the Potential Energy Calculator ************"
write(*, *) ""
write(*, *) "This program calculates the potential energy of two point charges or"
write(*, *) "atoms for a distance of 0.8A to 20.0A through 200 steps."
write(*, *) ""
write(*, *) "Please consider that the formulas for doing so are the following:"
write(*, *) ""
write(*, *) "* Total potential energy:"
write(*, *) ""
write(*, *) "  V   (r)  =  V  (r) + V  (r)"
write(*, *) "   tot         sr       lr"
write(*, *) ""
write(*, *) "* Short range potential:"
write(*, *) ""
write(*, *) "                                    2"
write(*, *) "                /     /-B /r - r \\\"
write(*, *) "                |     \   \     0//|"
write(*, *) "  V  (r) = D  . \1 - e             /  - D"
write(*, *) "   sr       e                            e"
write(*, *) ""
write(*, *) "* Long range potential:"
write(*, *) ""
write(*, *) "             / A     D \"
write(*, *) "  V  (r') = -|--- - ---|"
write(*, *) "   lr        |  6     8|"
write(*, *) "             \r'    r' /"
write(*, *) ""
write(*, *) "  While considering that:"
write(*, *) "                /-/r - r \\"
write(*, *) "                \ \     0//"
write(*, *) "  r' = r + C . e"
write(*, *) ""
write(*, *) "The program will create an output file with the name output.csv"
write(*, *) "which you may read using Microsoft Excel, LibreOffice Calc or"
write(*, *) "similar spreadsheet software."
write(*, *) ""

! Estumating the size of the step
step = (max_val - init_val) / (steps - 1)

do i = 1, steps
	! Recalculate the distance of the charges and store it
	distance_vector(i) = init_val + step * (i - 1)
end do

! Check if the user provided a file with the parameters
write(*, *) '--------------------------------------------------------------------'
if (COMMAND_ARGUMENT_COUNT() == 1) then ! If so, extract the parameters from the file
	call GET_COMMAND_ARGUMENT(1,file_name)
	write(*, *) "Reading the parameters from the file: ", file_name
	call read_params(TRIM(file_name), De, A, B, C, D, r0)
else ! If not, use the defaults
	write(*, *) "The program will work with the default set of parameters."
	De = 4.618
	A = 7.500
	B = 1.869
	C = 5.000
	D = 125.0
	r0 = 1.275
end if
! Show all the parameters, so that the user knows what the actual input was
write(*, *) "De = ", De
write(*, *) "A  = ", A
write(*, *) "B  = ", B
write(*, *) "C  = ", C
write(*, *) "D  = ", D
write(*, *) "r0 = ", r0
write(*, *) '--------------------------------------------------------------------'

! The calculations are carried out here
write(*, *) 'The potential energy is being calculated ...'
! Compute the short range interaction
call short_range(distance_vector, De, B, r0, Vsr)
! Compute the long range interaction
call long_range(distance_vector, A, C, D, r0, Vlr)
! Add both interactions and store the resulting value
energy_vector = Vsr + Vlr

write(*, *) 'The potential energy has been calculated successfully!'
write(*, *) 'Writing the results to the output.csv file ...'

! Write the output to the file using both: flotating point and exponential formats --------
open(44, file='output.csv', status='replace')
write(44, *) 'Distance(A)     Energy(eV)'
do i = 1, steps
	write(44, "(' ', 1F5.2, 1E26.8)") distance_vector(i), energy_vector(i)
end do
close(44)
write(*, *) 'File created successfully!'
write(*, *) ''
write(*, *) '********************************************************************'
write(*, *) 'The program has now finished.'
write(*, *) 'Have a nice day!'

end program exercise2
