
# Exercise 2 (0.1.3)

Last revision: Oct 16, 2021

---

This program solves the second exercise of the final project for the course\
*TÉCNICAS COMPUTACIONALES Y CÁLCULO NUMÉRICO* in the EMTCCM.

- **Author:** Rony J. Letona
- **email:** [rony.letona@estudiante.uam.es](mailto:rony.letona@estudiante.uam.es)

---

## 1. Installation

### a) Required software

This program has been compiled and tested using **GFortran**.\
Therefore, we suggest that you compile the source code using GFortran in a\
unix-like environment. If you don't have such an environment, please have a\
look at the [Installing GFortran](https://fortran-lang.org/learn/os_setup/install_gfortran)\
article in the Fortran official page.

To check if you have GFortran installed in your unix-like environment, please\
run the following command in a Terminal:

`which gfortran`

If the result isn't a path, please install GFortran using one of the following:

- Debian-based Linux: `sudo apt-get install gfortran`
- RedHat-based Linux: `sudo yum install gcc-gfortran` or\
`sudo dnf install gcc-gfortran`
- Arch Linux: `sudo pacman -S gcc-fortran`
- macOS: (if you have XCode installed) `xcode-select --install`\
otherwise `port search gcc && sudo port install gcc10`

### b) Source code

The source code for this program should be included with this README, and it\
should be comprised of 2 Fortran -f90- files:

- exercise2.f90
- energy.f90

The additional files and directories are used to compile the program, explain\
how to install and run the program, and to run some tests.

- **Parameters**
- README.txt
- makefile
- tests.sh

### c) GFortran and Make versions

Before compiling the program, it would be a good idea to check the version of\
GFortran and Make. This program was compiled and tested using GFortran 9.3.0 and\
GNU Make 4.2.1. To check your GFortran and Make versions, please run the following\
commands in a Terminal.

`gfortran --version`

`make --version`

### d) Installation

Finally, if you have GFortran installed in your system, and the version is\
comparable, then please open a Terminal window in the folder with the files,\
and run the following command:

`make`

Please note that the only files in that folder should be the ones listed in\
section 1. b) of this README. Otherwise, *make* may fail to compile the program.

Please note that this program uses subroutines. Therefore, if you wish to\
compile the program from the source code, I suggest that you first check the\
*makefile*.

Additionally, regarding the subroutines, it should be pointed out that these\
were designed to handle the calculations in the program, leaving the program\
with the input/output operations. For more information about this, please check\
the source code.

## 2. Running the program

### a) Tests

Before running the program, some tests might be in order to make sure everything\
is working fine. To do so, please run the following command in a Terminal window\
inside the folder with the files:

`./tests.sh`

As a result, you should see 100 messages corresponding to all 100 tests you\
just made. If you see any error message stating that the program has FAILED,\
please try running the following command and start over with the installation\
as described in secion 1. d)

`make clean`

On the other hand, if you wish to check the results of each test, you may do\
so by going into the newly generated **Tests** directory and select one of the\
folders in it. Each one will contain the original parameters file, a log file\
with the visual output of the program, and the _ouput.csv_ file with the results.\
To compare if all the results are in order, a **References** directory has\
been included in the main directory; it contains the results of running the\
`tests.sh` script successfully.

Finally, if you find any errors that you can't explain or fix by re-compiling\
the program, please contact the author; the details are provided at the start\
of this README.

### b) Input file

*Exercise 2* may run on its own without any argument in a Terminal. However,\
if a filename is specified right after running the program, then the file\
will be read looking for the parameters (for more information about the\
parameters, please check sections 2. f) and 2. g) in this README).

If a file is provided, then the format for said file should be a single column\
with 6 entries, each one for a single parameter. It should look something like\
this:

4.618\
7.5\
1.869\
5.0\
125.0\
1.275

The list of parameters is this: **De**, **A**, **B**, **C**, **D**, **r0**. You should never change\
the order of the parameters, for this list is fixed in the program. The\
latter will only read said parameters in that particular order. The file\
name for the file with the parameters is arbitrary, since the program will\
take any name as argument. Nevertheless, I recommend that you provide your\
file with a meaningful name (e.g. `params.dat`). For more information about\
the parameters and the theory behind them, please have a look at\
section 2. f) and 2. g) in this README.

### c) Output file

*Exercise 2* will produce a single output file named `output.csv` The file\
should have a title row with a description of both columns and then 200 rows\
with the data for the inter-particle/inter-atomic distance and the computed\
energy at that distance. Both columns will be separated by spaces.

### d) Running it

Once you have compiled the program, you may proceed to run the following command:

`./exercise2.exe`

Or, if you wish to specify different parameters:

`./exercise2.exe params.dat`

Where `params.dat` is the name of the file with the data for the parameters.

If you get any error from running this program, please refer to sections 1. a)\
and 1. d). Otherwise, please contact the author.

#### e) Example

To run a quick test, it is suggested that you try the following:

1. Create a new empty file called `params.dat`
2. Open the file and fill it with the following matrix

5.0\
8.0\
2.0\
6.0\
120.0\
1.5

3. Save the file and close it
4. Make sure that both files are in the same directory as `exercise2.exe`
5. Open a Terminal window and change your directory to the one with the files\
and the program.
6. Run the program by typing the following in the Terminal and pressing enter:

`./exercise2.exe params.dat`

7. The program should give some feedback about the calculations it carried out,\
but the most important line is the one telling you that:

`The program has now finished.`

Additionally, the program should have created a file called **output.csv**. This\
file contains the results of running the program with this set of parameters. To\
check if the program has carried out everything correctly, an **Example** directory\
has been included with the reference CSV file, for you to compare your results.

#### f) Parameters and Results
The parameters of the program may be changed after the program has been compiled.\
Before running it, the user may provide the program with a file name as an argument\
in the Terminal, so that the program reads that file and extracts the parameters\
from it. If the user doesn't provide a file name, then the program will do the\
calculations with the default set of parameters:

De = 4.618\
A = 7.500\
B = 1.869\
C = 5.000\
D = 125.0\
r0 = 1.275

Finally, the results of the calculation are stored in a CSV file as described in\
section 2. c) of this README.

### g) Theory behind it

This small program is designed to compute the potential energy between an atom\
and a charge or molecule using a Morse potential and a special case of a\
Lennard-Jones 6-8 potential. The sum of both potentials yields a total energy\
in *eV* which describes the behavior of a chemical non-covalent bond at\
different distances of both atom andcharge/molecule. The program generates an\
output of the potential energy for certain inter-atomic distances (from 0.8A\
to 20.0A in 200 steps). Since different atoms/charges/molecules would behave\
differently, the parameters for the potential energy can be changed by the user.\
We will now describe this in detail:

#### i. Potential Energy
The total energy of the interaction is computed by adding a Short-Range potential\
and a Long-Range potential as follows:

```math
V_{Tot} \left( r \right) = V_{SR} \left( r \right) + V_{LR} \left( r \right)
```

Where $`r`$ is the distance between the atom and the charge/molecule.

#### ii. Morse (Short-Range) Potential
The Morse potential was defined using the following formula:

```math
V_{SR} \left( r \right) = D_{e} \left( 1 - e^{\left( - B \left( r - r_{0} \right) \right)} \right)^2 - D_{e}
```

In this case, $`D_{e}`$, $`B`$ and $`r_{0}`$ are parameters that can be modified by\
the user for specific cases of the different interactions.

#### iii. Lennard-Jones 6-8 (Long-Range) Potential
Finally, the special Lennard-Jones 6-8 potential is defined in the following\
way:

```math
V_{LR} \left( r' \right) = - \left( \frac{A}{r'^{6}} - \frac{D}{r'^{8}} \right)
```

Please note that this formula doesn't work with the regular distance $`r`$.\
In this case, $`r'`$ is defined as follows:

```math
r' = r + C \cdot e^{- \left( r - r_{0} \right)}
```

As with the Morse potential, $`A`$, $`D`$, $`C`$ and $`r_{0}`$ are parameters that can\
be modified by the user for specific cases of the different interactions.

## 3. FAQs

1. *Can I run the program in Microsoft Windows?* - If you install Cygwin or enable\
the WSL environment, you should be able to compile and run the program. However,\
we do suggest that you have a look at section 1. a) of this README. GNU Fortran\
is highly recommended to compile the code.

2. *I deleted an important file. What do I do?* - If you deleted a file, please\
follow this link to locate the project's repository: [https://gitlab.com/zronyj/emtccm-fortran-e2](https://gitlab.com/zronyj/emtccm-fortran-e2)

3. *The compilation failed and I have a lot of strange files in my folder.* - In\
this case, please proceed to remove all files with `.o` and `.mod` file extensions\
and try to compile the program again. If this fails, please check sections 1. a)\
and 1. c) of this README.

4. *How do I read the **output.csv** file?* - This file is just a text file with\
the data written in two separate columns. You may read it using Microsoft Excel,\
LibreOffice Calc, Google Sheets, etc. When doing so, please make sure that you\
choose _spaces_ as separators and, if possible, tick the choice to merge separators.

5. *Does the program use any input file?* - The program doesn't require an input\
file to work, but if you wish to change the parameters of the program in order to\
obtain different results, then you do have to use an input file as described in\
sections 2. b) and 2. f) of this README.

6. *I used a parameter file and the program just crashed. What do I do?* - First\
of all: don't panic. If the program crashed while using a parameter file, you\
probably entered another non-numeric character or a blank space before or after\
some number in the file. I suggest that you run the program again and make sure\
that you did not enter any additional characters for the parameters.

## 4. Acknowledgements

Special thanks go to *Ania Beatriz Rodriguez* and *Orla Mary Gleeson* for making\
me laugh and making my evenings so entertaining while writing all this code. And\
a special thanks to *David Varas* for sharing additional information about how the\
program should work.

## 5. License

Copyright 2021 Rony J. Letona

Permission is hereby granted, free of charge, to any person obtaining a copy of\
this software and associated documentation files (the "Software"), to deal in the\
Software without restriction, including without limitation the rights to use, copy,\
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,\
and to permit persons to whom the Software is furnished to do so, subject to the\
following conditions:

The above copyright notice and this permission notice shall be included in all\
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL\
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING\
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS\
IN THE SOFTWARE.